package com.example.gitapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootApplication
public class GitApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitApiApplication.class, args);
    }
}